let tasks = [];

document.getElementById('addTaskButton').addEventListener('click', () => {
    addTask();
});

document.getElementById('markTaskButton').addEventListener('click', () => {
    markTaskAsComplete();
});

function addTask() {
    let taskDescription = document.getElementById('taskDescription').value;
    if (taskDescription) {
        tasks.push({ description: taskDescription, completed: false });
        document.getElementById('taskDescription').value = '';
        displayTasks();
    }
}

function markTaskAsComplete() {
    let taskIndex = document.getElementById('taskIndex').value;
    taskIndex = parseInt(taskIndex);
    if (taskIndex >= 0 && taskIndex < tasks.length) {
        tasks[taskIndex].completed = true;
        document.getElementById('taskIndex').value = '';
        displayTasks();
    } else {
        alert("Invalid task index.");
    }
}

function displayTasks() {
    let taskList = document.getElementById("taskList");
    taskList.innerHTML = "";
    tasks.forEach((task, index) => {
        taskList.innerHTML += `
            <li class="${task.completed ? 'completed' : ''}">
                <span class="task-index">${index}.</span> ${task.description}
            </li>`;
    });
}